**Docker** is a tool designed to make it easier to create, deploy, and run applications by using containers. 
Containers allow a developer to package up an application with all of the parts it needs, such as libraries and other dependencies, and deploy it as one package

**Docker commands**

**docker pull**
this command is used to pull images from the docker repository
$ docker pull <image_name>

**docker run**
this command is used to create a container from an image
$ docker run -it -d <image_name>

**docker ps**
this command is used to list the running containers
$ docker ps

**docker ps-a**
this command is used to show all the running and exited containers

$ docker ps -a

**docker commit**
this command creates a new image of an edited container on the local system

$ docker commit <container_id><user_name/image_name>

**docker login**
the command is used to login to the docker hub repository
$ docker login

**docker push**
this command is used to push an image to the docker hub repository
$ docker push <username/image_name>

**docker build**
this command is used to build an image from a specified docker file
$ docker build<path to docker file>

**docker rm**
this command is used to delete a stopped container
$ docker rm <container_id>
