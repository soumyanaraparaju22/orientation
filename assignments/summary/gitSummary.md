**Git**
Git is an opensource  distributed version-control system which is used for tracking the changes in a source code during software development. It  it useful for coordinating work between programmers.

**Git Commands**

**git config**
the command matches the user name and email address to be used with their commits
$ git config --global user.name "user_name"
$ git config --global user.email "user_email"

**git push**
the command sends the commited changes of the master branch to the user remote repository
$ git push variable_name master

**git branch**
the command lists all the local branches in the current repository
$ git branch
to create a new branch
$ git branch [branch_name]
to delete the feature branch
$ git branch -d [branch_name]

**git merge**
the command merges the specified branch's history into the current branch
$ git merge [branch_name]

**git clone**
this command is used to obtain a repository from a existing URL
$ git clone[url]

**git fork**
creating a fork is producing a personal copy of someone else's project. 

**git workflow**
$ git clone <repository-url>
$ git checkout master
$ git checkout -b <user_branch_name>
$ git add .
$ git commit -sv
$ git push origin <branch_name>










   